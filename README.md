

# Import vehicles API challenge 
### by Luciano Rodriguez for Patagonian

## summary: 
- architecture: Hexagonal / Ports and Adaptes
- stack: Node.js (TypeScript), SQlite3, Knex, NGINX, inversify, Jest, Super
- environment: Docker (Linux)

## requirements:
  - docker and docker compose is required
  - you need the ports 5555 and 8080 available
  - this docker-compose has been tested running on ubuntu

## notes:
 - the backend code is in ./backend/src/app
 - sqlite3 database is in persistence / db and can be inspected with some tool
 - api.raml and api.html are at the root of the project
 - the import is executed by batch of 50 rows, and if any failure returns an error and does not continue.
- It is configured with two valid providers: "ford" and  "generic"

 ## Import configuration and behavior

 The import configuration file is in 
 /backend/src/config/providers.json, it follows a JSON Schema, which is recognized by the VScode IDE (for validation and suggestions)

for each field:

You must indicate the order number (source property) of the column (starting from 0) that corresponds in the csv.

You must indicate whether or not it is required by the provider (required property), if a value is not provided for a field and it was indicated that it is not required, it will try to fill in a default value, otherwise it will fail.

A sample.csv file is provided.


# instructions

  - run docker-compose:
  
  ```bash
    $ docker-compose up --build
  ```
  - enter the bash of the "api" container (in other terminal, without stopping what we started in the previous step):

  ```bash
    $ docker-compose exec api bash
  ```
  - once inside, make surethat you are working in /app/src and next:

  ```bash
    $ npm run migrate:latest
  ```
  - Run tests

   ```bash
    $ npm run test
  ```
  - You are ready to test the API in http://localhost:5555,
  ```bash
    $ curl --location --request POST 'http://localhost:5555/vehicles/import' \
--form 'file=@"/home/dev/Descargas/sample.csv"' \
--form 'provider="ford"'
  ```

## documentation

  - For api documentation and classes/methods reference see in docs folder


## clean up
If it gets stuck or something is going seriously wrong, it might be worth cleaning the bins and starting over.

- ```$ docker-compose down```
- ```$ sudo rm -rf ./persistence```
- ```$ docker-compose up --build```
