import { injectable } from "inversify";
import { v4 as uuidv4 } from 'uuid';

import fs from 'fs';
import * as path from 'path';
import { Vehicle } from "../../domain/entities/vehicle";
import { ImportSerializeService } from "../../domain/services/impor-serialize-service";
import { ProviderNotFound, RequiredColumnNotFound } from "../../domain/errors/import-errors";
import { EntityFields, IPItem, IPLayout } from "../types/importer";

const PATH_FILE = path.resolve(__dirname, "../../../config/providers.json");

/**
 * A concrete vehicle serializer
 * takes into account the provider configuration file
 *
 */

@injectable()
export class ConfigImportSerializeService implements ImportSerializeService{
    private providers: IPItem[];

    constructor() {
        const rawdata = fs.readFileSync(PATH_FILE, 'utf-8');
        this.providers = JSON.parse(rawdata).providers;
    }

    /**
     * Take into account the providers
     * in the configuration file
     *
     */
    listProviders(): string[] {
        return this.providers.map(p => p.provider);
    }
    
     /**
     * Take into account the layout by provider
     * in the configuration file
     *
     * @returns a vehicle
     */
    serialize(d: any, provider: string): Vehicle {
        const l = this._getProvider(provider);
        return {
            uuid: this._uuid(d, l),
            createDate: this._createDate(d, l),
            updateDate: this._updateDate(d, l),
            make: this._make(d, l),
            vin: this._vin(d, l),
            year: this._year(d, l),
            model:  this._model(d, l),
            mileage: this._mileage(d, l),
            price: this._price(d, l),
            zipCode: this._zipCode(d, l),
        }
    }

    private _def<T>(d: any, l: IPLayout, f: EntityFields, fb: T): T {
        const field = l[f];
        const value = d[field.source];
        if (value !== undefined && value !== '') {
            return value;
        }
        if (field.required) {
            throw new RequiredColumnNotFound(f);
        }
        return fb;
    }

    private _uuid(d: any, l: IPLayout): string {
        const field = l[EntityFields.uuid];
        const value = d[field.source];
        if (value !== undefined) {
            return value;
        }
        if (field.required) {
            throw new RequiredColumnNotFound(EntityFields.uuid);
        }
        return uuidv4();
    }

    private _model(d: any, l: IPLayout): string {
        return this._def<string>(d, l, EntityFields.model, 'unknow');
    }

    private _make(d: any, l: IPLayout): string {
        return this._def<string>(d, l, EntityFields.make, 'unknow');
    }

    private _vin(d: any, l: IPLayout): string {
        return this._def<string>(d, l, EntityFields.vin, 'unknow');
    }

    private _year(d: any, l: IPLayout): number | null {
        return this._def<number | null>(d, l, EntityFields.year, null);
    }

    private _mileage(d: any, l: IPLayout): number | null {
        return this._def<number | null>(d, l, EntityFields.mileage, null);
    }

    private _price(d: any, l: IPLayout): number | null {
        return this._def<number | null>(d, l, EntityFields.price, null);
    }

    private _zipCode(d: any, l: IPLayout): string {
        return this._def<string>(d, l, EntityFields.zip_code, 'unknow');
    }

    private _createDate(d: any, l: IPLayout): Date {
        const field = l[EntityFields.create_date];
        const value = d[field.source];
        if (value !== undefined) {
            return new Date(value);
        }
        if (field.required) {
            throw new RequiredColumnNotFound(EntityFields.create_date);
        }
        return new Date();
    }

    private _updateDate(d: any, l: IPLayout): Date {
        const field = l[EntityFields.update_date];
        const value = d[field.source];
        if (value !== undefined) {
            return new Date(value);
        }
        if (field.required) {
            throw new RequiredColumnNotFound(EntityFields.update_date);
        }
        return new Date();
    }

    private _getProvider(name: string): IPLayout {
        const item = this.providers
            .find(i => i.provider === name);
        if (!item) {
            throw new ProviderNotFound();
        }
        return item.layout;

    }

}
