/**
 * Represents a vehicle in the database
 */
export interface VehicleRow {
    uuid: string;
    vin: string;
    make: string;
    model: string;
    mileage: number | null;
    year: number | null;
    price: number | null;
    zip_code: string;
    create_date: string;
    update_date: string;
}