import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('vehicles', function(table) {
        table.increments('id').primary();
        table.string('uuid', 64).notNullable();
        table.string('vin', 64).notNullable();
        table.string('make').notNullable();
        table.string('model').notNullable();
        table.integer('mileage');
        table.integer('year');
        table.double('price');
        table.string('zip_code').notNullable();
        table.timestamp('create_date').defaultTo(knex.fn.now());
        table.timestamp('update_date').defaultTo(knex.fn.now());
        table.unique(['uuid']);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('vehicles');
}
