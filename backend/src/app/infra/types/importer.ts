export interface IPFile {
    providers: IPItem[];
    [key: string]: any;
}

export interface IPField {
    required: boolean;
    source: number;
}

export enum EntityFields {
    uuid = 'uuid',
    vin = 'vin',
    make = 'make',
    model = 'model',
    mileage = 'mileage',
    year = 'year',
    price = 'price',
    zip_code = 'zip_code',
    create_date = 'create_date',
    update_date = 'update_date',
}

export interface IPLayout {
    uuid: IPField;
    vin: IPField;
    make: IPField;
    model: IPField;
    mileage: IPField;
    year: IPField;
    price: IPField;
    zip_code: IPField;
    create_date: IPField;
    update_date: IPField;
}

export interface IPItem {
    provider: string;
    layout: IPLayout;
}