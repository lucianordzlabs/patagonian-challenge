import { injectable } from "inversify";

import knex, { Knex } from 'knex';
import * as knexconfig from '../knexfile'
const environment = process.env.ENVIRONMENT || 'development'

@injectable()
export class KnexConn {
    private knex: Knex<any, unknown[]>;

    constructor() {
        this.knex = knex(knexconfig[environment]);
    }

    get() {
        return this.knex;
    }
}
  