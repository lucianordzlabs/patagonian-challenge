import { Response, Request, NextFunction } from "express";
import { injectable } from "inversify";
import { VehicleImportFileIsRequired, VehicleImportProviderIsNotValid } from "./vehicle-import-controller";

@injectable()
export class ErrorHandleController {

    /**
     * Handle errors and send the appropriate response
     *
     * @param err - error instance
     * @param req - request handler
     * @param res - response handler
     * @param next - middleware handler
     * 
     * @returns response to send

    */
    execute(err: Error, req: Request, res: Response, next: NextFunction): Response {

        if (err instanceof VehicleImportProviderIsNotValid) {
            return res.status(400).send({
                "message": err.message,
            });
        }
      
        if (err instanceof VehicleImportFileIsRequired) {
            return res.status(400).send({
                "message": err.message,
            });
        }
    
        return res.status(500).send({
            "message": 'internal error',
        });
    }


}
