import fs from 'fs';
import { inject, injectable } from "inversify";
import { Response, Request, NextFunction } from "express";
import { ImportRowUseCase } from "../../application/use-cases/import-row-use-case";
import { parseFile } from 'fast-csv';
import { ListProvidersUseCase } from "../../application/use-cases/list-providers-use-case";

const ROWS_BATCH = 50;

/**
 * Handle response for vehicle import 
 */
@injectable()
export class VehicleImportController {

    /**
     * @param useCase - import frow csv row use case (application service)
     * @param listProvidersUseCase - list providers use case (application service)
     * 
    */
    constructor(
      @inject(ImportRowUseCase) private useCase: ImportRowUseCase,
      @inject(ListProvidersUseCase) private listProvidersUseCase: ListProvidersUseCase,
    ) { }
    
    /**
     * Handle response for vehicle import 
     *
     * @param err - error instance
     * @param req - request handler
     * @param res - response handler
     * 
     * @returns A void promise

    */
    async execute(req: Request, res: Response, next: NextFunction): Promise<void> {
        const provider = req.body.provider;
        let closed = false;
        let fileRows: any[] = [];

        try {
            await this._ensureProviderIsValid(provider);
            await this._ensureFileExist(req);

        } catch (error) {
            return next(error);
        }

        const file = (req as MulterRequest).file;


        parseFile(file.path)
            .on("data", async (data) => {
                if (closed) {
                    return;
                }
                fileRows = [...fileRows, data];
                if (fileRows.length >= ROWS_BATCH) {
                    try {
                        await this.useCase.execute(provider, fileRows);
                    } catch (error) {
                        closed = true;
                        next(error);
                    }
                }
            })
            .on("end", async () => {
                if (closed) {
                    return;
                }
                try {
                    await this.useCase.execute(provider, fileRows);
                    fs.unlinkSync(file.path);
                    this._sendResults(res);
                } catch (error) {
                    closed = true;
                    next(error);
                }
            })
    }

    private _ensureFileExist(req: Request) {
        if (!(req as MulterRequest).file) {
            throw new VehicleImportFileIsRequired();
        }
    }

    private _sendResults(res: Response) {
        res.json({ message: 'success' });
    }

    private async _ensureProviderIsValid(provider: string): Promise<void> {
        const result = await this.listProvidersUseCase.execute();
        if (!result.includes(provider)) {
            throw new VehicleImportProviderIsNotValid();
        }
    }

}

export class VehicleImportProviderIsNotValid extends Error {
    name = 'vehicle import provider is not valid';
    message = 'provider field is not valid'
}

export class VehicleImportFileIsRequired extends Error {
    name = 'vehicle import file is required';
    message = 'file field is required'
}

interface MulterRequest extends Request {
    file: any;
}
