import { injectable, inject } from "inversify";
import { KnexVehicleSerializer } from "../serializers/knex-vehicle-serializer";
import { KnexConn } from "../knex-conn";
import { Vehicle } from "../../domain/entities/vehicle";
import { OperationalError } from "../../domain/errors/operational-error";
import { VehicleRepository } from "../../domain/repositories/vehicle-repository";

/**
 * concrete implementation of vehicle repository
 */
@injectable()
export class KnexVehicleRepository implements VehicleRepository {

    /**
     * @param knexConn - an interface connects to the DB
     * 
     */
    constructor(
        @inject(KnexConn) private knexConn: KnexConn,
    ) { }
    
    /**
     * persistence for vehicles
     */
    async add(items: Vehicle[]): Promise<void> {
        try {
            const rows = items.map(i => KnexVehicleSerializer.toInternal(i));
            await this.knexConn.get()('vehicles')
            .insert(rows)
            .onConflict('uuid')
            .merge();
    
        } catch (error) {
            console.error(error);
            throw new OperationalError()
        }
    }

}