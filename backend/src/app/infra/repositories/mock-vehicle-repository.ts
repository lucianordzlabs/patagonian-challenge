import { injectable } from "inversify";
import { Vehicle } from "../../domain/entities/vehicle";
import { VehicleRepository } from "../../domain/repositories/vehicle-repository";

@injectable()
export class MockVehicleRepository implements VehicleRepository {
    spy = {
        add: {
            times: 0,
            lastArguments: [] as Vehicle[],
            retVal: undefined
        }
    }
    async add(item: Vehicle[]): Promise<void> {
      this.spy.add.times += 1;
      this.spy.add.lastArguments = item;
      return Promise.resolve(this.spy.add.retVal);
    }
}
