import { inject, injectable } from "inversify";
import { URLSearchParams, URL } from 'url';


@injectable()
export class ParseQueryService {

    execute(reqUrl: string): URLSearchParams {
        var url = new URL(reqUrl, 'http://localhost');
        return new URLSearchParams(url.search);
    }

}
  