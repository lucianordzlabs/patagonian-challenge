import { Vehicle } from "../../domain/entities/vehicle"
import { VehicleRow } from "../rows/vehicle-row";


export class KnexVehicleSerializer {

    /**
     * Given a vehicle in db format 
     * return a vehicle in the domain.
     * 
     * @param row - a vehicle in db format
     *
     */
    static toRepresentation(row: VehicleRow): Vehicle {
        return {
            uuid: row.uuid,
            vin: row.vin,
            make: row.make,
            mileage: row.mileage,
            model: row.model,
            price: row.price,
            year: row.year,
            zipCode: row.zip_code,
            updateDate: new Date(row.update_date),
            createDate: new Date(row.create_date),
        };
    }

    /**
     * Given a vehicle in domain return 
     * a vehicle in the db format
     * 
     * @param item - a vehicle the domain
     *
     */
    static toInternal(item: Vehicle): VehicleRow {
        return {
            uuid: item.uuid,
            vin: item.vin,
            make: item.make,
            mileage: item.mileage,
            model: item.model,
            price: item.price,
            year: item.year,
            zip_code: item.zipCode,
            update_date: item.updateDate.toISOString().slice(0, 19).replace('T', ' '),
            create_date: item.createDate.toISOString().slice(0, 19).replace('T', ' '),
        }
    }


}