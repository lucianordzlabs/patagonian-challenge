/**
 * Represents a vehicle in the domain
 */
export interface Vehicle {
    uuid: string;
    vin: string;
    make: string;
    model: string;
    mileage: number | null;
    year: number | null;
    price: number | null;
    zipCode: string;
    createDate: Date;
    updateDate: Date;
}