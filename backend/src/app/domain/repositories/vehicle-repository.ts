import { Vehicle } from "../entities/vehicle";

/**
 * Contract for vehicle repositories
 */
export abstract class VehicleRepository {

    /**
     * persistence for vehicles
     */
    abstract add(item: Vehicle[]): Promise<void>;
}
