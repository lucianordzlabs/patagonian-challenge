export interface DataCollection<T> {
    total: number,
    data: T[]
}