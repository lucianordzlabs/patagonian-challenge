export class ProviderNotFound extends Error {
    name = 'provider not found';
}

export class RequiredColumnNotFound extends Error {
    constructor(col: string) {
        super();
        this.name = `column ${col} not found on row`;
    }
}