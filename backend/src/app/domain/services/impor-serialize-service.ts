import { Vehicle } from "../entities/vehicle";

/*
 * the contract for the service that
 *  serializes according to the provider's layout
 * 
 */
export abstract class ImportSerializeService {
    /*
     * returns registered providers
     */
    abstract listProviders(): string[];

    /*
     * given a raw data and a supplier, returns a vehicle
     */
    abstract serialize(data: any, provider: string): Vehicle;
}