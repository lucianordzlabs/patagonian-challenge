import * as path from 'path';

const BASE_PATH = __dirname;

module.exports = {

  development: {
    client: 'sqlite3',
    connection: {
        filename: path.resolve(path.join(BASE_PATH, '..', '..', 'db'), 'db.sqlite3')
    },
    migrations: {
      directory: path.join(BASE_PATH, 'infra', 'migrations'),
    },
    seeds: {
      directory: path.join(BASE_PATH, 'infra', 'seeds'),
    },
  },

}