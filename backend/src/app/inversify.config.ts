import "reflect-metadata";
import { Container } from "inversify";
import { KnexConn } from "./infra/knex-conn";
import { ImportRowUseCase } from "./application/use-cases/import-row-use-case";
import { VehicleImportController } from "./infra/controllers/vehicle-import-controller";
import { ListProvidersUseCase } from "./application/use-cases/list-providers-use-case";
import { VehicleRepository } from "./domain/repositories/vehicle-repository";
import { KnexVehicleRepository } from "./infra/repositories/knex-vehicle-repository";
import { ImportSerializeService } from "./domain/services/impor-serialize-service";
import { Router } from "express";
import { ErrorHandleController } from "./infra/controllers/error-handler-controller";
import { ConfigImportSerializeService } from "./infra/infrastructure.module";

const serviceContainer = new Container();

// controllers
serviceContainer.bind<VehicleImportController>(VehicleImportController).to(VehicleImportController);
serviceContainer.bind<ErrorHandleController>(ErrorHandleController).to(ErrorHandleController);
// use cases
serviceContainer.bind<ImportRowUseCase>(ImportRowUseCase).to(ImportRowUseCase);
serviceContainer.bind<ListProvidersUseCase>(ListProvidersUseCase).to(ListProvidersUseCase);
// repositories
serviceContainer.bind<VehicleRepository>(VehicleRepository).to(KnexVehicleRepository);
// others
serviceContainer.bind<ImportSerializeService>(ImportSerializeService).to(ConfigImportSerializeService)
serviceContainer.bind<KnexConn>(KnexConn).to(KnexConn);

export { serviceContainer };
