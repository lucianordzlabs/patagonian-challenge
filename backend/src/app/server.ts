import express, { Express, NextFunction, Request, Response, Router } from 'express';
import { Container } from 'inversify';
import multer from 'multer';
import { ErrorHandleController } from './infra/controllers/error-handler-controller';
import { VehicleImportController, VehicleImportFileIsRequired, VehicleImportProviderIsNotValid } 
from './infra/controllers/vehicle-import-controller';


export function createServer(container: Container, port?: number): Express {
  const app = express();

  const vehicleImport = container.get<VehicleImportController>(VehicleImportController);
  const errorHandler = container.get<ErrorHandleController>(ErrorHandleController);

  var upload = multer({ dest: '/tmp/csv/' });
  
  app.post('/vehicles/import', upload.single('file'), 
    (req: Request, res: Response, next: NextFunction) => {
    void vehicleImport.execute(req, res, next);
  });
  
  app.use(function (err: Error, req: Request, res: Response, next: NextFunction){
    void errorHandler.execute(err, req, res, next);

  });
  
  if (port) {
    app.listen(port, () => {
      return console.log(`server is listening on ${port}`);
    });
  }
  
  app.use(express.urlencoded({ extended: true }));

  return app;
}
