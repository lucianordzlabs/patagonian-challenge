export class ListProvidersError extends Error {
    name = 'operational error when listing providers';
}