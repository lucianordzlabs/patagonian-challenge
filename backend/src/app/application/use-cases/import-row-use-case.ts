import { injectable, inject } from "inversify";
import { ProviderNotFound, RequiredColumnNotFound } from "../../domain/errors/import-errors";
import { OperationalError } from "../../domain/errors/operational-error";
import { VehicleRepository } from "../../domain/repositories/vehicle-repository";
import { ImportSerializeService } from "../../domain/services/impor-serialize-service";
import { ImportRowError } from "../errors/import-row-error";

@injectable()
export class ImportRowUseCase {

    /**
     * @param repo - the concrete repository (DI) 
     * of vehicles useful to persist vehicles
     * 
     * @param serializer - the concrete serializer (DI)
     * useful to convert raw row to Vehicle entities
     *
     */
    constructor(
        @inject(VehicleRepository) private repo: VehicleRepository,
        @inject(ImportSerializeService) private serializer: ImportSerializeService,
    ) { }

    /**
     * execute import frow csv row
     * 
     * @param provider - a valid provider name
     * @param data - a raw object from a csv row
     *
     */
    async execute(provider: string, data: any[]): Promise<void> {
        try {
            const vehicles = data.map( d => this.serializer.serialize(d, provider))
            await this.repo.add(vehicles);
        } catch (error) {
            throw this._handleError(error);
        }
    }

    private _handleError(error: Error) {
        if (error instanceof OperationalError) {
            return new ImportRowError();
        }
        if (error instanceof RequiredColumnNotFound) {
            return new ImportRowError();
        }
        if (error instanceof ProviderNotFound) {
            return error;
        }
        return error;
    }
}