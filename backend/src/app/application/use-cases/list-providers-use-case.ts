import { injectable, inject } from "inversify";
import { OperationalError } from "../../domain/errors/operational-error";
import { ImportSerializeService } from "../../domain/services/impor-serialize-service";
import { ListProvidersError } from "../errors/list-providers-error";

@injectable()
export class ListProvidersUseCase {

    /**
     * @param serializer - the concrete serializer (DI)
     * useful to convert raw row to Vehicle entities
     *
     */
    constructor(
        @inject(ImportSerializeService) private serializer: ImportSerializeService,
    ) {}
    
    /**
     * @returns a list of providers registered 
     * in the configuration file
     * 
     */
    async execute(): Promise<string[]> {
        try {
            const results = this.serializer.listProviders();
            return results;
        } catch (error) {
            throw this._handleError(error);
        }
    }

    private _handleError(error: Error) {
        if (error instanceof OperationalError) {
            return new ListProvidersError();
        }
    }
}