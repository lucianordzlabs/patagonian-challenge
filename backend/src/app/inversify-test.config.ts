import "reflect-metadata";
import { Container, injectable } from "inversify";
import { KnexConn } from "./infra/knex-conn";
import { ImportRowUseCase } from "./application/use-cases/import-row-use-case";
import { VehicleImportController } from "./infra/controllers/vehicle-import-controller";
import { ListProvidersUseCase } from "./application/use-cases/list-providers-use-case";
import { VehicleRepository } from "./domain/repositories/vehicle-repository";
import { ImportSerializeService } from "./domain/services/impor-serialize-service";
import { MockVehicleRepository } from "./infra/repositories/mock-vehicle-repository";
import { Router } from "express";
import { ErrorHandleController } from "./infra/controllers/error-handler-controller";
import { ConfigImportSerializeService } from "./infra/infrastructure.module";

const serviceContainer = new Container();

serviceContainer.bind<VehicleImportController>(VehicleImportController).to(VehicleImportController);
serviceContainer.bind<ErrorHandleController>(ErrorHandleController).to(ErrorHandleController);
serviceContainer.bind<ImportRowUseCase>(ImportRowUseCase).to(ImportRowUseCase);
serviceContainer.bind<ListProvidersUseCase>(ListProvidersUseCase).to(ListProvidersUseCase);
serviceContainer.bind<VehicleRepository>(VehicleRepository).to(MockVehicleRepository);
serviceContainer.bind<ImportSerializeService>(ImportSerializeService).to(ConfigImportSerializeService)
serviceContainer.bind<KnexConn>(KnexConn).to(KnexConn);
serviceContainer.bind<Router>(Router).toConstantValue(Router());

export { serviceContainer };
