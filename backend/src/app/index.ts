import { serviceContainer } from "./inversify.config";
import { createServer } from "./server";

if (process.env.NODE_ENV !== 'test') {
  createServer(serviceContainer, 3000);
}

