import "reflect-metadata";
import path from 'path';
import request from 'supertest';
import { VehicleRepository } from '../../../domain/repositories/vehicle-repository';
import { serviceContainer } from '../../../inversify-test.config';
import { createServer } from '../../../server';
import { MockVehicleRepository } from "../../../infra/repositories/mock-vehicle-repository";


serviceContainer.unbind(VehicleRepository)
serviceContainer.bind<VehicleRepository>(VehicleRepository).toConstantValue(new MockVehicleRepository());
const repo = serviceContainer.get<VehicleRepository>(VehicleRepository) as MockVehicleRepository;

const app = createServer(serviceContainer)


describe('vehicle import controller', () => {

  it('request should return provider field error', async (done) => {

    const res = await request(app)
    .post('/vehicles/import')
    .field('provider', 'aInvalidProvider');

    expect(res.status).toBe(400);
    expect(res.body).toStrictEqual({message: "provider field is not valid"});
    done();

  });

  it('request should return file field error', async (done) => {

    const res = await request(app)
    .post('/vehicles/import')
    .field('provider', 'ford');

    expect(res.status).toBe(400);
    expect(res.body).toStrictEqual({message: "file field is required"});
    done();

  });

  it('when api is calld with a correct request then repo should be called with vehicles', async (done) => {

    repo.spy.add.times = 0;

    const res = await request(app)
    .post('/vehicles/import')
    .field('provider', 'ford')
    .attach('file', path.resolve(__dirname, 'sample.csv'))
     expect(res.body).toStrictEqual({message: "success"});

    expect(repo.spy.add.times).toBe(1);
    expect(repo.spy.add.lastArguments[0].uuid).toBe('4f0886f1-b699-4c9a-8f4c-a823b43e8924');
    expect(res.status).toBe(200);
    done();

  });

});

