module.exports = {
  clearMocks: true,
  roots: ['<rootDir>/app/tests'],
  testEnvironment: 'node',
  preset: 'ts-jest',
  testMatch: ["**/?(*.)+(spec|test).ts"],
};